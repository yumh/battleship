#ifndef SERVER_H
#define SERVER_H

// This is the interface for our event-driven server

#include <netinet/in.h>
#include <sys/socket.h>

// define the types of events
enum event { CONN, IO };

enum action_type { NOP, CLOSE };

// define an action that the server will execute
struct action {
  enum action_type type;
  int fd;
};

// A simple vector of `struct action'
struct actions {
  int len;
  struct action *l;
};

// The option to initialise the server
struct server_opt {
  int port;                                                        // the desired port
  int max_clients;                                                 // maximum number of parallel clients
  int max_pending;                                                 // max number of pending clients
  struct actions *(*react)(enum event, int, struct sockaddr_in *); // how to "react" when an event occurs
};

// start the server
void server_start(struct server_opt *);

// create an empty vector of actions. Is responsibility of the caller
// to free the memory by calling `destroy_actions'
struct actions *init_actions();

// free the memory
void destroy_actions(struct actions *);

// easy create a vector of action with only one action (CLOSE or NOP)
struct actions *easy_action(int, enum action_type);
struct actions *easy_close(int);
struct actions *easy_nop(int);

#endif
