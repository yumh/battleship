#ifndef GAME_H
#define GAME_H

// enumerate all the possible types of cells
enum cell_type { UNKNOWN, WATER, SHIP, SHIP_HITTED };

// define a board
struct board {
  int size;                     // the size, a board is a square
  enum cell_type *cells;        // a list of cells
  int ship_cells;               // the number of cells occupied by ships
  int ship_cells_hitted;        // the number of ship fragment hitted
};

// define a player
struct player {
  char *nick;                   // its null-terminated nickname
  int fd;                       // the file descriptor of the player
  struct room *belongs_to;      // the room he belongs to
};

// define a list of players
struct players {
  int len;                      // its length
  struct player *players;       // the list
};

// define a room
struct room {
  enum { FST, SND } round;      // whose turn?
  struct player *p1;            // the first player
  struct player *p2;            // the second player
  struct board player1_board;   // the p1 board
  struct board player2_board;   // the p2 board
};


// === board related ===

// create a board given a size and initialise it to UNKNOWN
struct board *create_board(int);

// destroy a board
void destroy_board(struct board *);

// hit a cell (given by coordinates) on a given board and return its
// content. Mind that this could change the board content.
enum cell_type hit_cell(struct board *, int, int);


// === player related ===

// create a player given a nickname
struct player *create_player(char *, int);

// destroy a user. It'll free also the nickname
void destroy_player(struct player*);

// create the list of player
struct players *create_players();

// push a player into the list
void players_push(struct players *, struct player *);

// Find and return the first ready player. It'll return `null` if no
// one is ready
struct player *pop_ready_player(struct players *);

// destroy the list of players
void destroy_players(struct players *);


// == Room related ==

// create a room. Will modify the two player
struct room *create_room(struct player *, struct player *);

// destroy a room. It won't free the players
void destroy_room(struct room *);

#endif
