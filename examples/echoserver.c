#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "../server.h"
#include "../protocol.h"

struct actions *react(enum event e, int fd, struct sockaddr_in *address) {
  int r;
  char buffer[512];
  char *welcome     = "ECHO server v1.0. Welcome!\ntype 'quit' to quit\n";
  char *cmex        = "quit";
  unsigned int clen = strlen(cmex);

  switch (e) {
  case CONN:
    send(fd, welcome, strlen(welcome), 0);
    return easy_nop(fd);

  case IO:
    if ((r = read(fd, buffer, sizeof(buffer))) < 1)
      return easy_close(fd);

    if ((unsigned)r >= clen && strncmp(buffer, cmex, clen) == 0)
      return easy_close(fd);

    buffer[r] = '\0';
    send(fd, buffer, r, 0);
    return easy_nop(fd);
  }
}

int main() {
  struct server_opt opt = { .port = 8080,
                            .max_clients = 30,
                            .max_pending = 10,
                            .react = &react };
  printf("Starting the ECHO SERVER v1.0 on port %d...\n", opt.port);
  server_start(&opt);
}
