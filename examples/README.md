# Examples

## `echoserver`

``` shell
make echoserver
./echoserver

# on anther terminal
telnet localhost 8080
```

A simple server that echoes back the messages you send.
