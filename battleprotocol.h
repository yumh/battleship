#ifndef PROTOCOL_H
#define PROTOCOL_H

// A definition of a interface for protocols. Still WIP

#include "server.h"

struct request {
};

struct response {
  
};

// handle a request. The first parameter is the file descriptor of the
// incoming socket. The second parameter is a pointer to a callback
// function that return void and take a `struct response' as only
// parameter.
void acquire(const int *, void (*)(const struct request *));

// send data back to the client
void ship(const struct response *);

#endif
