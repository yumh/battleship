CC=clang
CFLAGS=-O0 -g -Wall
LDFLAGS=

.PHONY: all battleship echoserver clean

all: battleship

battleship: battleship.c server.o battleprotocol.o game.o utils.o
	$(CC) $(CFLAGS) $(LDFLAGS) \
		battleship.c server.o battleprotocol.o game.o utils.o \
		-o battleship

server.o: server.c server.h utils.h
	$(CC) $(CFLAGS) -c server.c -o server.o

game.o: game.c game.h utils.h
	$(CC) $(CFLAGS) -c game.c -o game.o

battleprotocol.o: battleprotocol.c battleprotocol.h
	$(CC) $(CFLAGS) -c battleprotocol.c -o battleprotocol.o

utils.o: utils.c utils.h
	$(CC) $(CFLAGS) -c utils.c -o utils.o

echoserver: examples/echoserver.c server.o
	$(CC) $(CFLAGS) $(LDFLAGS) \
		examples/echoserver.c server.o -o echoserver

clean:
	@echo "Cleaning..."
	@rm -f server.o
	@rm -f battleprotocol.o
	@rm -f battleship
	@rm -f echoserver
	@rm -f utils.o
	@rm -f game.o
	@echo "Done!"
