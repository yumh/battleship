#include <stdlib.h>

#include "game.h"
#include "utils.h"

struct board *create_board(int size) {
  struct board *b = scalloc(1 , sizeof(struct board));
  b->size = size;
  b->cells = scalloc(size, sizeof(enum cell_type));
  return b;
};

void destroy_board(struct board *b) {
  free(b->cells);
  free(b);
}

enum cell_type hit_cell(struct board *b, int x, int y) {
  if (x > b->size || y > b->size) {
    return UNKNOWN;
  }

  int s = x + y*x;
  enum cell_type *target = b->cells + s;
  if (*target == SHIP) {
    *target = SHIP_HITTED;
  }

  return *target;
}

struct player *create_player(char *nick, int fd) {
  struct player *p = scalloc(1, sizeof(struct player));
  p->nick = nick;
  p->fd = fd;
  return p;
}

void destroy_player(struct player *p) {
  free(p->nick);
  free(p);
}
