#include "utils.h"

void *smalloc(size_t s) {
  void *t = malloc(s);
  if (t == NULL) exit(EX_OSERR);
  return t;
}

void *scalloc(size_t n, size_t s) {
  void *t = calloc(n, s);
  if (t == NULL) exit(EX_OSERR);
  return t;
}
