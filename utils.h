#ifndef UTILS_H
#define UTILS_H

#include <stdlib.h>
#include <sysexits.h>

// safe version of the {m,c}alloc function of stdlib
void *smalloc(size_t);
void *scalloc(size_t, size_t);
void *srealloc(void *, size_t);

#endif
