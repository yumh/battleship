#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sysexits.h>

#include "server.h"
#include "utils.h"

void handle(const int *fd, const int *max_clients, int *clients, struct actions *todo) {
  for (int i = 0; i < todo->len; i++) {         // iterate over the action(s)
    if ((i + todo->l)->type == CLOSE) {         // if we need to close something
      close(*fd);                               // close it
      for (int i = 0; i < *max_clients; i++) {  // and then remove from our clients list
        if (clients[i] == *fd) {
          clients[i] = 0;
          break;
        }
      }
    }
  }
  destroy_actions(todo);                        // finally free memory
}

void server_start(struct server_opt *opts) {
  int *clients = calloc(opts->max_clients, sizeof(int));
  if (clients == NULL) {
    perror("calloc");
    exit(EX_OSERR);
  }

  // one socket to rule them all
  int master_socket;
  if ((master_socket = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
    perror("socket failed");
    exit(EX_OSERR);
  }

  // master socket can handle multiple connections
  int reuse = 1;
  if (setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(int)) < 0) {
    perror("setsockopt");
    exit(EX_OSERR);
  }

  struct sockaddr_in address;
  size_t addrlen = sizeof(address);
  address.sin_family            = AF_INET;
  address.sin_addr.s_addr       = INADDR_ANY;
  address.sin_port              = htons(opts->port);

  // bind the socket
  if (bind(master_socket, (struct sockaddr *)&address, addrlen) < 0) {
    perror("bind");
    exit(EX_OSERR);
  }

  // listen
  if (listen(master_socket, opts->max_pending) < 0) {
    perror("listen");
    exit(EX_OSERR);
  }

  while (1) {
    // later we'll need the highest fd number for select(2)
    int max_sd = master_socket;

    // create an empty set of fds
    fd_set readfds;
    FD_ZERO(&readfds);

    // add master socket to the set
    FD_SET(master_socket, &readfds);

    // iterate through our clients list and add the active ones to the
    // set
    for (int i = 0; i < opts->max_clients; i++) {
      int sd = clients[i];
      if (sd > 0) {
        FD_SET(sd, &readfds);
      }

      // calculate the highest fd number, we'll needed it later for
      // select(2)
      if (sd > max_sd) {
        max_sd = sd;
      }
    }

    // wait for an activity on one of our sockets. timeout = NULL so
    // we wait indefinitely
    int activity = select(max_sd +1, &readfds, NULL, NULL, NULL);
    if (activity < 0) {
      perror("select");
    }

    // maybe it's a new client!
    if (FD_ISSET(master_socket, &readfds)) {
      int new_socket;
      if ((new_socket = accept(master_socket, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
        perror("accept");
        exit(EX_OSERR);
      }

      // handle the new client
      struct actions *todo = opts->react(CONN, new_socket, &address);
      handle(&new_socket, &opts->max_clients, clients, todo);

      // push our new client to the list of sockets
      for (int i = 0; i < opts->max_clients; i++) {
        if (clients[i] == 0) {
          clients[i] = new_socket;
          break;
        }
      }
    }

    // check if some IO happened on other sockets
    for (int i = 0; i < opts->max_clients; i++) {
      int sd = clients[i];

      if (FD_ISSET(sd, &readfds)) {
        // this could be incoming data from user, as well as a
        // disconnected user
        struct actions *todo = opts->react(IO, sd, &address);
        handle(&sd, &opts->max_clients, clients, todo);
      }
    }
  }

  // the funny part is that the execution will never reach this point
  return;
}

struct actions *init_actions() {
  struct actions *res   = calloc(1, sizeof(struct actions));
  if (res == NULL) perror("calloc"), exit(EX_OSERR);
  return res;
}

void destroy_actions(struct actions *a) {
  if (a->l != NULL) free(a->l);
  free(a);
}

struct actions *easy_action(int fd, enum action_type t) {
  struct actions *res = init_actions();
  res->l = malloc(sizeof(struct action));
  if (res->l == NULL) perror("malloc"), exit(EX_OSERR);
  res->len = 1;
  res->l->fd = fd;
  res->l->type = t;
  return res;
}

struct actions *easy_close(int fd) {
  return easy_action(fd, CLOSE);
}

struct actions *easy_nop(int fd) {
  return easy_action(fd, NOP);
}
