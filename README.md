# Battleship et al

## TL;DR

**Status**: highly wip, far from being complete

## What's inside?

### abstract

This aim to be a multiplayer battleship game for *NIX OS. Its based on
our event-driven server and uses its own protocol for client-server
communication.

This is written to somewhat skill with the C language, the network
programming and for fun.

### server

Our server, as said, is event-driven. ATM is build around the
`select(2)` syscall, so we're managing multiple clients with one
process. Maybe in a near future we'll switch to a
multi-{process,thread,magic} approach.

The main idea is that our process sleeps and when a new client open a
connection, as well as some of our client start sending data, we wake
up and call a `react` function that takes the event and returns a list
of action to perform.

### examples

In the `examples` directory there are some examples to show how
different service can be built around our code.

## Build

If you are on a UNIX compliant OS (like *BSD or GNU/linux) is as
simple as:

``` shell
make
```

or

``` shell
make <example-name>
```
